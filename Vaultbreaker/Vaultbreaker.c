#include <stdio.h>

void UserFeedback (int guess, int code);

int main (void) {

int code = 4576; // hardwire code
int guess = -1;
int numberOfAttempts = 0;

  while(guess != code){
    numberOfAttempts++;
      printf("Guess the code: ");
      scanf("%d", &guess);

        if(guess==code)
          printf("You broke the vault! it took you %d tries"
                 " to break the vault!\n", numberOfAttempts);
        else
          UserFeedback(guess, code); // call to function that checks code
  }

return 0;

}

void UserFeedback(int guess, int code) {

// inputs values into an int array in which each digit

int guessDigits[4]={(guess/1000),(guess/100)%10,(guess/10)%10,guess%10};
int codeDigits[4]={(code/1000),(code/100)%10,(code/10)%10,code%10};

int equal = 0, lesser = 0, greater = 0;

printf("Nope! Your clue is: ");

// checks for inequalities and equality

  for(int i = 0; i < 4; i++){
    if(guessDigits[i] == codeDigits[i])
        equal++;
    if(guessDigits[i] < codeDigits[i])
        lesser++;
    if(guessDigits[i] > codeDigits[i])
        greater++;
  }

// prints the calculations

  for(int i = 0; i < equal; i++)
      printf("=");
  for(int i = 0;i < lesser; i++)
      printf("<");
  for(int i = 0; i < greater; i++)
      printf(">");

printf("\n");
}