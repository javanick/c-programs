#include "myHeader.h"

void printHeader( int width, int height, FILE *filePointer) {
	fprintf(filePointer, "P6\n");
	fprintf(filePointer, " %d %d 255\n", width, height );
}


// input the structure called theImage as an argument to function,
// and create a 2-d array of red, green, and blue as defined in the structure

void printImage (int rows, int cols, pixel theImage[rows][cols], FILE *filePointer) {
	for (int i=0; i < rows; i++){
		for (int k=0; k < cols; k++){
			fprintf(filePointer, "%c%c%c", theImage[i][k].r, theImage[i][k].g, theImage[i][k].b);
		}
	}

}