// Starts the Program

#include "myHeader.h"

int main(){

  FILE *filePointer;

  filePointer = fopen("/root/Desktop/Testing/tiger.ppm", "r");

  int columns;
  int rows;

  parseHeader(filePointer, &columns, &rows);
  pixel theImage[rows][columns];
  getImage(filePointer, rows, columns, theImage);
  grayScale(rows, columns, theImage);

  return 0;
}