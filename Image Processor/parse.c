/*
		parseHeader()
			gets width(cols) & height(rows) from header of image
		getImage()
			gets the image data and puts into the array (an array of structs)
			
		file pointer sent in to both - the file is openend in the main driver
			(transform.c)
*/

#include "myHeader.h"

// function prototypes
int strToInt (char array[]);


void parseHeader ( FILE *input, int *cols, int *rows )  {

	int i, j;
	char line[80], header[80], height[4], width[4];

 	if (fgets(header, 80, input) != NULL) {
		/* two cases:
			1.  1st line has P6 only on it
			2.  1st line has more than just P6 on it
		*/
		if (strlen(header) == 3) {  // only has P6 on it and a '\n' character
			fgets(line, 80, input);  // get next line
			if (line[0] == '#')      // if next line is a comment, get the next line
				fgets(line, 80, input);
		}
		else {  							 // has more than just P6 on it
			// copy everything after P6 into line[] array
			for (i = 3, j = 0; header[i] != '\0'; i++, j++)
				line[j] = header[i];
		}
	}

	// line[] array contains width and height
	for (i = 0; line[i] != ' ';  i++)  // parse out width
		width[i] = line[i];
	width[i] = '\0';
	*cols = strToInt(width);

	j = i + 1;
	if (line[j] != '\0') {
		for (i=0; line[j] != ' ' && line[j] != '\0'; j++, i++) {
			height[i] = line[j];  // parse out height
		}
		height[i] = '\0';
		*rows = strToInt(height);

		// if reached a space, then maximum pixel value is on next line
		if (line[j] != ' ') {
			// trying to get past the maximum pixel value
			fgets(line, 80, input);
			if (line[0] == '#')      // if next line is a comment, get the next line
				fgets(line, 80, input);
		}
	}
}

void getImage ( FILE *input, int rows, int cols, pixel theImage[rows][cols] )  {
	// fill up theArray with the pixel data
	int numChars = 0, i, j;
	int theChar1, theChar2, theChar3;

	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
	 		if ( ( (theChar1 = fgetc(input)) != EOF ) && (theChar1 != '\n') ){
				theImage[i][j].r = theChar1;
        //printf("TheChar1: %d\t Whats in the Array: %d\n", theChar1, theImage[i][j].r);
        numChars++;
      }
	 		if ( ( (theChar2 = fgetc(input)) != EOF ) && (theChar2 != '\n') ){
				theImage[i][j].g = theChar2;
        numChars++;
      }
	 		if ( ( (theChar3 = fgetc(input)) != EOF ) && (theChar3 != '\n') ) {
				theImage[i][j].b = theChar3;
        numChars++;
      }
		}
	}
}

int strToInt (char array[] )  {
	int i, intValue = 0, theValue = 0;

	for (i = 0; array[i] >= '0' && array[i] <= '9'; ++i) {
		intValue = array[i] - '0';
		theValue = theValue * 10 + intValue;
	}

	return theValue;
}