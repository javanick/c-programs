#include "myHeader.h"

// prints the contents of a file instead of standard output.
// contains pointer to a file.

// scans in a choice and returns that choice, which calls a function.

int printMenu( ) {
	int choice = 0; // initialized to zero
	while (choice < 1 || choice > 4) {
		fprintf(stderr,"\n1. original image\n");
		fprintf(stderr,"2. mirror image\n");
		fprintf( stderr,"3. upside down image\n");
		fprintf(stderr,"4. gray scale image\n");
		fprintf(stderr,"\nMENU CHOICE:   ");
		scanf("%d", &choice);
	}
	return choice;
}