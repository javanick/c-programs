#include "myHeader.h"

//  mirrors the rows along the x axis.


void flipHoriz( int rows, int cols, pixel theImage[rows][cols] ) {
 pixel temp;

 for (int i=0; i < rows / 2; i++){
   for (int j=0; j < cols; j++){
     temp = theImage[i][j];
     theImage[i][j] = theImage[rows - i - 1][j];
     theImage[rows - i - 1][j] = temp;
   }
 }


}
