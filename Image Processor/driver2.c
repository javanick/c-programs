/*

	transform.c file - the main driver
		first calls parseHeader() to get the width and height
		then declares 2-D array

		calls getImage() to get the pixel data
		then calls appropriate manipulation function:
			1: mirror image
			2: upside-down
			3: rotated to the right 90 degrees
		calls printHeader()
		then printImage()

	the makefile:
		expects the following files:
			transform.c
			parse.c
			print.c
			mirror.c
			flipHoriz.c
			rotate.c
		creates an executable called transform

	to run the program:
		transform Tigers.ppm

*/

#include "myHeader.h"


int main (int argc, char *argv[])  {

	//delcare int values that are arguments to functions

	int width, height, menuChoice;

// if number of arguments pointed to by argv is one then produce an error

	if (argc == 1) {
		fprintf (stderr, "<executable> <image_ name>\n");
		exit(1);
	}
// If there is no input file, print that the file cannot be opened.

	FILE *inputFile;
	inputFile = fopen( argv[1], "r" );


	if (inputFile == NULL) {
		fprintf (stderr, "File could not be opened. \n");
		exit(1);
	}

	parseHeader( inputFile, &width, &height);
	pixel image[height][width];
	// pixel imageCopy[height][width];
	// printf("height is %d, width is %d\n", height, width);
	getImage( inputFile, height, width, image );
	fclose(inputFile);

 	// printHeader( width, height );
  	// printImage( height, width, image );

	menuChoice = printMenu();
	// printf("menuChoice is %d\n", menuChoice);


	// manipulate the image according to menu choice
	//		1: original image
	//		2: mirror image
	//		3: upside down image
	//		4: gray scale image

	FILE *fp = fopen("NewTigers.ppm", "w");

	if (menuChoice == 1) {
		//Do Nothing lawl
  		printHeader( width, height, fp);
  		printImage( height, width, image, fp);
	}
	else if (menuChoice == 2) {
   	mirror( height, width, image );
			printHeader( width, height, fp);
			printImage( height, width, image, fp);
	}
   else if (menuChoice == 3) {
   	flipHoriz( height, width, image );
			printHeader( width, height, fp);
			printImage( height, width, image, fp);
	}
   else {
   	grayScale( height, width, image );
			printHeader( width, height, fp);
			printImage( height, width, image, fp);
	}
	return 0;
}