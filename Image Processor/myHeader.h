/* Starts the program by including all of the libraries
   and the structure we use for images (chars red, green, and blue)
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct{
  unsigned char r, g, b;
} pixel;

// functions we use in the program

void parseHeader ( FILE *input, int *cols, int *rows );
int strToInt (char array[] );
void getImage ( FILE *input, int rows, int cols, pixel theImage[rows][cols] );
void flipHoriz( int rows, int cols, pixel theImage[rows][cols] );
void mirror( int rows, int cols, pixel theImage[rows][cols] );
void grayScale( int rows, int cols, pixel theImage[rows][cols] );
int printMenu( );
void printHeader( int width, int height, FILE *filePointer);
void printImage (int rows, int cols, pixel theImage[rows][cols], FILE *filePointer);