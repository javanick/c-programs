#include "myHeader.h"

// multiply struct components by constants to change the colors.
// can add other filters to the menu

void grayScale( int rows, int cols, pixel theImage[rows][cols] ) {

int greyScale = 0;

	for (int i=0; i < rows; i++){
		for (int k=0; k < cols; k++){
			greyScale = (theImage[i][k].r * 30 / 100) +
									(theImage[i][k].g * 59 / 100) +
									(theImage[i][k].b * 11 / 100);
			theImage[i][k].r = greyScale;
			theImage[i][k].g = greyScale;
			theImage[i][k].b = greyScale;
		}
	}
}
