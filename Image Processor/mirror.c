#include "myHeader.h"

	//	for each row, go halfway, and swap the values

void mirror( int rows, int cols, pixel theImage[rows][cols] ) {
	pixel temp;
	int i, j;
	for (i=0; i < rows; i++){
		for (j=0; j < cols/2; j++){
			temp = theImage[i][j];
      theImage[i][j] = theImage[i][cols - j - 1];
      theImage[i][cols - j - 1] = temp;
		}
	}

}